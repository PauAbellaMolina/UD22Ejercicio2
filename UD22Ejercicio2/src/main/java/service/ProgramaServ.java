package service;

import javax.swing.JOptionPane;

import controller.ProgramaController;
import dao.UD22Dao;
import dto.UD22Dto;

public class ProgramaServ {
	
	public static boolean consultaPersona=false;
	public static boolean modificaPersona=false;
	
	//Aquí valido que la inserción del cliente se haya hecho como yo quiero
	public static void validarRegistroCliente(UD22Dto miCliente) {
		UD22Dao miClienteDao;
		if (miCliente.getIdcliente() > 1) {
			miClienteDao = new UD22Dao();
			miClienteDao.registrarCliente(miCliente);						
		}else {
			JOptionPane.showMessageDialog(null,"El número debe ser mayor a 1","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
	}
	//Aquí valido que la inserción del video se haya hecho como yo quiero
	public static void validarRegistroVideo(UD22Dto miVideo) {
		UD22Dao miVideoDao;
		if (miVideo.getIdvideos() > 1) {
			miVideoDao = new UD22Dao();
			miVideoDao.registrarVideo(miVideo);						
		}else {
			JOptionPane.showMessageDialog(null,"El número debe ser mayor a 1","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
	}
	//Aquí valido que la modificación del cliente se haya hecho como yo quiero
	public static void validarModificacionCliente(UD22Dto miCliente) {
		UD22Dao miClienteDao;
		if (miCliente.getNombre().length()>1) {
			miClienteDao = new UD22Dao();
			miClienteDao.modificarCliente(miCliente);	
			modificaPersona=true;
		}else{
			JOptionPane.showMessageDialog(null,"El nombre debe tener más de 1 digito","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaPersona=false;
		}
	}
	//Aquí valido que la modificación del video se haya hecho como yo quiero
	public static void validarModificacionVideo(UD22Dto miVideo) {
		UD22Dao miVideoDao;
		if (miVideo.getTitle().length()>1) {
			miVideoDao = new UD22Dao();
			miVideoDao.modificarCliente(miVideo);	
			modificaPersona=true;
		}else{
			JOptionPane.showMessageDialog(null,"El nombre debe tener más de 1 digito","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaPersona=false;
		}
	}
	//Aquí valido que la eliminación del cliente se haya hecho como yo quiero
	public static void validarEliminacionCliente(UD22Dto miClienteDelete) {
		UD22Dao miClienteDao=new UD22Dao();
		miClienteDao.eliminarCliente(miClienteDelete);
	}
	//Aquí valido que la eliminación del video se haya hecho como yo quiero
	public static void validarEliminacionVideo(UD22Dto miVideoDelete) {
		UD22Dao miVideoDao=new UD22Dao();
		miVideoDao.eliminarVideo(miVideoDelete);
	}
}


