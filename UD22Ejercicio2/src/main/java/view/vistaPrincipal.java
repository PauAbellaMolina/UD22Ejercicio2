package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//vista principal donde contendrá todas las posibles vistas a través de botones
public class vistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JLabel lblNewLabel;
	private JButton btnModificarCliente;
	private JButton btnEliminarCliente;
	private JButton btnInsertarVideo;
	private JButton btnModificarVideo;
	private JButton btnEliminarVideo;

	
	public vistaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 665, 497);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Insertar Cliente");
		
		lblNewLabel = new JLabel("Pagina principal");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel.setBounds(213, 37, 166, 45);
		contentPane.add(lblNewLabel);
		
		JButton btnInsertarCliente = new JButton("Insertar Cliente");
		btnInsertarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProgramaController.mostrarVistaInsertarCliente();
			}
		});
		btnInsertarCliente.setBounds(29, 124, 195, 23);
		contentPane.add(btnInsertarCliente);
		
		btnModificarCliente = new JButton("Modificar Cliente");
		btnModificarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProgramaController.mostrarVistaUpdateCliente();
			}
		});
		btnModificarCliente.setBounds(29, 159, 195, 23);
		contentPane.add(btnModificarCliente);
		
		btnEliminarCliente = new JButton("Eliminar Cliente");
		btnEliminarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProgramaController.mostrarVistaDeleteCliente();
			}
		});
		btnEliminarCliente.setBounds(29, 193, 195, 23);
		contentPane.add(btnEliminarCliente);
		
		btnInsertarVideo = new JButton("Insertar Video");
		btnInsertarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProgramaController.mostrarVistaInsertarVideo();
			}
		});
		btnInsertarVideo.setBounds(365, 124, 195, 23);
		contentPane.add(btnInsertarVideo);
		
		btnModificarVideo = new JButton("Modificar Video");
		btnModificarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProgramaController.mostrarVistaUpdateVideo();
			}
		});
		btnModificarVideo.setBounds(365, 159, 195, 23);
		contentPane.add(btnModificarVideo);
		
		btnEliminarVideo = new JButton("Eliminar Video");
		btnEliminarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProgramaController.mostrarVistaDeleteVideo();
			}
		});
		btnEliminarVideo.setBounds(365, 193, 195, 23);
		contentPane.add(btnEliminarVideo);
	}



	
}
