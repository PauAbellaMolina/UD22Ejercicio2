package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;
import dto.UD22Dto;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;


//vista del menú usado para modificar campos de la base de datos del cliente
public class vistaUpdateCliente extends JFrame {
	
	private JPanel contentPane;
	private JTextField textFieldid;
	private JLabel lblNombre;
	private JTextField textFieldnombre;
	private JLabel lblApellido;
	private JTextField textFieldapellido;
	private JLabel lblDireccion;
	private JTextField textFielddireccion;
	private JLabel lblDNI;
	private JTextField textFielddni;
	private JLabel lblFecha;
	private JTextField textField;
	private JLabel lblTitulo;
	private JButton btnBuscarID;
	private JButton btnUpdate;
	private JButton btnUpdate_1;
	private ProgramaController clienteController;

	public vistaUpdateCliente() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Update Cliente");
		
		JLabel lblid = new JLabel("ID");
		lblid.setBounds(34, 109, 31, 14);
		contentPane.add(lblid);
		
		textFieldid = new JTextField();
		textFieldid.setBounds(75, 106, 86, 20);
		contentPane.add(textFieldid);
		textFieldid.setColumns(10);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(25, 140, 45, 14);
		contentPane.add(lblNombre);
		
		textFieldnombre = new JTextField();
		textFieldnombre.setColumns(10);
		textFieldnombre.setBounds(75, 137, 86, 20);
		contentPane.add(textFieldnombre);
		
		lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(25, 171, 45, 14);
		contentPane.add(lblApellido);
		
		textFieldapellido = new JTextField();
		textFieldapellido.setColumns(10);
		textFieldapellido.setBounds(75, 168, 86, 20);
		contentPane.add(textFieldapellido);
		
		lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(191, 105, 60, 14);
		contentPane.add(lblDireccion);
		
		textFielddireccion = new JTextField();
		textFielddireccion.setColumns(10);
		textFielddireccion.setBounds(261, 102, 86, 20);
		contentPane.add(textFielddireccion);
		
		lblDNI = new JLabel("DNI");
		lblDNI.setBounds(191, 141, 60, 14);
		contentPane.add(lblDNI);
		
		textFielddni = new JTextField();
		textFielddni.setColumns(10);
		textFielddni.setBounds(261, 138, 86, 20);
		contentPane.add(textFielddni);
		
		lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(191, 171, 60, 14);
		contentPane.add(lblFecha);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(261, 168, 86, 20);
		contentPane.add(textField);
		
		lblTitulo = new JLabel("Update cliente");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTitulo.setBounds(146, 11, 137, 34);
		contentPane.add(lblTitulo);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UD22Dto miCliente=new UD22Dto();
				miCliente.setIdcliente(Integer.parseInt(textFieldid.getText()));
				miCliente.setNombre(textFieldnombre.getText());
				miCliente.setApellido(textFieldapellido.getText());
				miCliente.setDireccion(textFielddireccion.getText());
				miCliente.setDni(textFielddni.getText());
				miCliente.setFecha(Integer.parseInt(textField.getText()));
				
				ProgramaController.modificarCliente(miCliente);
			}
		});
		btnUpdate.setBounds(159, 227, 89, 23);
		contentPane.add(btnUpdate);
		
	}
}
