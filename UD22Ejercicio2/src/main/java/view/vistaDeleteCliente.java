package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;
import dto.UD22Dto;
import java.awt.event.ActionListener;

public class vistaDeleteCliente extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldid;
	private JLabel lblDelete;
	private JButton btnEliminar;

	//vista del menú para eliminar un cliente
	public vistaDeleteCliente() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Delete Cliente");
		
		JLabel lblid = new JLabel("ID");
		lblid.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblid.setBounds(36, 64, 29, 20);
		contentPane.add(lblid);
		
		textFieldid = new JTextField();
		textFieldid.setBounds(75, 66, 252, 20);
		contentPane.add(textFieldid);
		textFieldid.setColumns(10);
		
		lblDelete = new JLabel("Delete cliente");
		lblDelete.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDelete.setBounds(146, 11, 137, 34);
		contentPane.add(lblDelete);
		
		btnEliminar = new JButton("Eliminar fila");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UD22Dto miClienteDelete=new UD22Dto();
				miClienteDelete.setIdcliente(Integer.parseInt(textFieldid.getText()));
				ProgramaController.eliminarCliente(miClienteDelete);
			}
		});
		btnEliminar.setBounds(146, 193, 137, 23);
		contentPane.add(btnEliminar);
	}
}
