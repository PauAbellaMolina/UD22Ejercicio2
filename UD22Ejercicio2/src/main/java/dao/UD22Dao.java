package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import conexion.Conexion;
import view.*;
import dto.UD22Dto;

public class UD22Dao {

	//método que usaré para insertar un video a la base de datos
	public void registrarVideo(UD22Dto miVideo) {
		Conexion conex= new Conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO videos VALUES ('"+miVideo.getIdvideos()+"', '"
					+miVideo.getTitle()+"', '"+miVideo.getDirector()+"', '"
					+miVideo.getCli_id()+"');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}
	//método que usaré para insertar un cliente a la base de datos
	public void registrarCliente(UD22Dto miCliente) {
		Conexion conex= new Conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO cliente VALUES ('"+miCliente.getIdcliente()+"', '"
					+miCliente.getNombre()+"', '"+miCliente.getApellido()+"', '"
					+miCliente.getDireccion()+"','"+miCliente.getDni()+"','"+miCliente.getFecha()+"');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}
	//método que usaré para modificar un video en base de datos
	public void modificarVideo(UD22Dto miVideo) {
		
		Conexion conex= new Conexion();
	    try {
	    	Statement st = conex.getConnection().createStatement();
	        String sql= "UPDATE videos SET title = '" + miVideo.getTitle() + "', director = '" + miVideo.getDirector() + "', cli_id = '" + miVideo.getCli_id() + "' WHERE id = " + miVideo.getCli_id() + ";";
	        st.executeUpdate(sql);
	        JOptionPane.showMessageDialog(null, "El video se ha actualizado correctamente","Update",JOptionPane.INFORMATION_MESSAGE);
	        st.close();
	        conex.desconectar();
	        
	    } catch (SQLException e) {
	        System.out.println(e.getMessage());
	        JOptionPane.showMessageDialog(null, "No se ha actualizado el video");
	    }
	}
	//método que usaré para modificar un cliente en base de datos
	public void modificarCliente(UD22Dto miCliente) {
	
	Conexion conex= new Conexion();		
    try {
    	Statement st = conex.getConnection().createStatement();
        String sql= "UPDATE cliente SET nombre = '" + miCliente.getNombre() + "', apellido = '" + miCliente.getApellido() + "', direccion = '" + miCliente.getDireccion() + "', dni = '" + miCliente.getDni() + "', fecha = '" + miCliente.getFecha() + "' WHERE id = " + miCliente.getIdcliente() + ";";
        st.executeUpdate(sql);
        JOptionPane.showMessageDialog(null, "El cliente se ha actualizado correctamente","Update",JOptionPane.INFORMATION_MESSAGE);
        st.close();
        conex.desconectar();
        
    } catch (SQLException e) {
        System.out.println(e.getMessage());
        JOptionPane.showMessageDialog(null, "No se ha actualizado el cliente");
    }
		
}
	//método que usaré para eliminar un video de la base de datos
	public void eliminarVideo(UD22Dto miVideoDelete) {
	Conexion conex= new Conexion();
	try {
		String sql= "DELETE FROM videos WHERE id='"+miVideoDelete.getIdvideos()+"'";
		Statement st = conex.getConnection().createStatement();
		st.executeUpdate(sql);
        JOptionPane.showMessageDialog(null, "Eliminado correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
        System.out.println(sql);
		st.close();
		conex.desconectar();
		
	} catch (SQLException e) {
        System.out.println(e.getMessage());
		JOptionPane.showMessageDialog(null, "No se Elimino");
	}
}
	//método que usaré para eliminar un cliente de la base de datos
	public void eliminarCliente(UD22Dto miClienteDelete) {
	Conexion conex= new Conexion();
	try {
		String sql= "DELETE FROM cliente WHERE id='"+miClienteDelete.getIdcliente()+"'";
		Statement st = conex.getConnection().createStatement();
		st.executeUpdate(sql);
        JOptionPane.showMessageDialog(null, "Eliminado correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
        System.out.println(sql);
		st.close();
		conex.desconectar();
		
	} catch (SQLException e) {
        System.out.println(e.getMessage());
		JOptionPane.showMessageDialog(null, "No se Elimino");
	}
}

	
}
